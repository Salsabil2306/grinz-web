export default [
    {
      'title': 'My Today Task',
      'allDay': true,
      'start': new Date(2021, 13, 0),
      
    },
    {
      'title': 'Long Event',
      'start': new Date(2021, 1, 7),
      'end': new Date(2021, 1, 7)
      
    },
  
    {
      'title': 'DTS STARTS',
      'start': new Date(2021, 2, 20),
      'end': new Date(2021, 2, 20)
    },
  
    {
      'title': 'DTS ENDS',
      'start': new Date(2016, 10, 16),
      'end': new Date(2016, 10, 16)
    },
  
    {
      'title': 'Some Event',
      'start': new Date(2021, 1, 9),
      'end': new Date(2021, 1, 9)
    },
    {
      'title': 'Conference',
      'start': new Date(2021, 1, 11),
      'end': new Date(2021, 1, 11),
      desc: 'Big conference for important people'
    },
    {
      'title': 'Meeting',
      'start': new Date(2021, 1, 18),
      'end': new Date(2021, 1, 18),
      desc: 'Pre-meeting meeting, to prepare for the meeting'
    },
    {
      'title': 'Lunch',
      'start': new Date(2021, 1, 18),
      'end': new Date(2021, 1, 18, 2, 0, 0),
      desc: 'Power lunch'
    },
    {
      'title': 'Meeting',
      'start': new Date(2021, 1, 18),
      'end': new Date(2021, 1, 18)
    },
    {
      'title': 'Happy Hour',
      'start': new Date(2021, 1, 18),
      'end': new Date(2021, 1, 18),
      desc: 'Most important meal of the day'
    },
    {
      'title': 'Dinner',
      'start': new Date(2021, 1, 17),
      'end': new Date(2021, 1, 17)
    },
    {
      'title': 'Birthday Party',
      'start': new Date(2021, 1, 11),
      'end': new Date(2021, 1, 11)
    },
    {
      'title': 'Birthday Party 2',
      'start': new Date(2021, 1, 11),
      'end': new Date(2021, 1, 11)
    },
    {
      'title': 'Birthday Party 1',
      'start': new Date(2021, 1, 11),
      'end': new Date(2021, 1, 11)
    },
    {
      'title': 'Late Night Event',
      'start': new Date(2021, 1, 17),
      'end': new Date(2021, 1, 17)
    },
    {
      'title': 'Multi-day Event',
      'start': new Date(2021, 1, 23),
      'end': new Date(2021, 1, 23 )
    }
  ]
  
  
  