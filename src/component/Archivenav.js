import React from 'react'
import friend from '../friends.svg'
import task from '../mytask.svg'
import './Archivenav.css'


function Archivenav() {
    return (
        <div>
            <ul className="archivemenu">
                      
                      <li><img src={task} alt="tsk"/>My Task</li>
                      <li><img src={friend} alt="frn"/>Friend Task</li>
                      

              </ul> 
            
        </div>
    )
}

export default Archivenav
