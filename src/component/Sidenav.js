import React, {useEffect} from 'react';
import style from './Sidenav.module.css'
import { Link } from 'react-router-dom';
import { useLocation } from "react-router-dom";
import archive from '../archived.svg'
import cal from '../calender.svg'
import friend from '../friends.svg'
import task from '../mytask.svg'

function Sidenav(props) {
    const currentRoute = useLocation().pathname.toLowerCase();
    console.log(currentRoute);
    
   
    
    
    
    
    if(props.sidebar===true){
        return (
        
            <div className={style.sidenav}>
                <ul>
                    <div className={style.username}><img className={style.profile} src={props.user.avatar}/>{props.user.name}</div>
                    <Link className={currentRoute.includes("mytask") ? style.tab : ""} to='./mytask'><li><img src={friend} alt="frn"/><div className="text">My Task</div></li></Link>
                    <Link className={currentRoute.includes("friendtask") ? style.tab : ""} to='./friendtask'><li><img src={task} alt="task"/><div className="text">Friends Task</div></li></Link>
                    <Link className={currentRoute == "/month" || currentRoute == "/week" ? style.tab : ""} to='/month'><li><img src={cal} alt="cal"/><div className="text">Calender</div></li></Link>
                    <Link className={currentRoute.includes("archive") ? style.tab : ""} to='/archive'><li><img src={archive} alt="archive"/><div className="text">Archived</div></li></Link>
                    
                </ul>  
              </div>
              
        );
    }
    else{
        return(
        <div className={style.sidenavHide}>     
            <ul>
            <div className={style.username}><img className={style.profile} src={props.user.avatar}/></div>
                <Link to='./mytask'><li><img src={friend} alt="frn"/></li></Link>
                <Link to='./home'><li><img src={task} alt="task"/></li></Link>
                <Link to='/month'><li><img src={cal} alt="cal"/></li></Link>
                <Link to='/archive'><li><img src={archive} alt="archive"/></li></Link>
                
            </ul>
        </div>
)

        
    }
    
}

export default Sidenav;