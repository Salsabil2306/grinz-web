import React from 'react'
import style from './Mytaskmenu.module.css'
import Overdue from '../logos/1.svg'
import Today from '../logos/2.svg'
import Tomorrow from '../logos/3.svg'
import Later from '../logos/4.svg'
import Completed from '../logos/5.svg'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

export default function Mytaskmenu() {
    return (
        <div>
              <ul className={style.mytaskmenu}>
                 <div></div>
                 <li><img src={Overdue} alt="tsk"/> Overdue</li>
                 <li><img src={Today} alt="tsk"/> Today</li>
                 <li><img src={Tomorrow} alt="tsk"/> Tomorrow</li>
                 <li><img src={Later} alt="tsk"/> Later</li>
                 <li><img src={Completed} alt="tsk"/> Completed</li>
                 

             </ul> 
            
        </div>
    )
}
