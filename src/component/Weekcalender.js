import {useState, useEffect} from 'react';
import { Calendar, momentLocalizer  } from 'react-big-calendar' 
import moment from 'moment'
import wevents from '../component/wevents'
import './Weekcalender.css'
import Toolbar from 'react-big-calendar/lib/Toolbar';
import Calnav from './Calnav'
import style from './Calnav.module.css'
import cal from '../calender.svg'
import friend from '../friends.svg'
import task from '../mytask.svg'
import filter from '../Filter.svg'
import next from '../nextMonth.svg'
import prev from '../prevMonth.svg'

function Weekcalender(props) {
    const localizer = momentLocalizer(moment);
    console.log(props.contactList);
    const taskList = props.mytask.map(x => ({ start: moment(x.date, 'YYYY-MM-DD').toDate(), end: moment(x.date, 'YYYY-MM-DD').toDate(), title: x.name, time: moment(x.date, 'HH:mm').toDate(), owner: x.assignBy }));
    const ftaskList = props.friendtask.map(x => ({ start: moment(x.date, 'YYYY-MM-DD').toDate(), end: moment(x.date, 'YYYY-MM-DD').toDate(), title: x.name, time: moment(x.date, 'HH:mm').toDate(), owner: x.assignTo[0] })); 
    const [eventarr, setEventarr] = useState([...taskList]);
    const [active, setActive]=useState(0);
    console.log(ftaskList);
  
    
  
    let match = [];
    eventarr.forEach(data => {
      props.contactList.map(e => {
        if (e.id === data.owner) {
          match.push({
            name: e.name,
            title: data.title,
            start: data.start,
            end: data.end,
            time: data.time,
            img: (e.avatar === "" ? "https://www.w3schools.com/howto/img_avatar2.png" : e.avatar  )          
          })
        }
      })
    })

    console.log(match);

  /* const events = []
        dummyData.forEach(obj => {
        obj.taskData.forEach(
            item => {
            events.push({
                start: moment(item.date),
                end: moment(item.date),
                title: obj.title
            })
            }
        )
        })
 */

        

        const HandleClick = (event) => {
            event.preventDefault();
            window.location.href='/month';
        
        
          }
        
        const CustomToolbar = (toolbar) => {
            const goToBack = () => {
              toolbar.date.setMonth(toolbar.date.getMonth() - 1);
              toolbar.onNavigate('PREV');
              setActive(2);
            };
          
            const goToNext = () => {
              toolbar.date.setMonth(toolbar.date.getMonth() + 1);
              toolbar.onNavigate('NEXT');
              setActive(3);
            };
          
            const goToCurrent = () => {
              const now = new Date();
              toolbar.date.setMonth(now.getMonth());
              toolbar.date.setYear(now.getFullYear());
              toolbar.onNavigate('current');
            };
          
            const label = () => {
              const date = moment(toolbar.date);
              return (
                <span><b>{date.format('MMMM')}</b><span> {date.format('YYYY')}</span></span>
              );
            };

            const mytaskarray = () => {
             match.forEach(element => {
                const taskDates=element.start;
                console.log(taskDates);
                const res = taskList.filter(singleTask => singleTask.start  === taskDates);
               
              
              }); 
              
              setEventarr(taskList);
              setActive(0);
            }
      


            const friendtaskarray = () => {
              setEventarr(ftaskList);
              setActive(1);

            }
      
            console.log(eventarr);

            return (
              <div >
                <div className="main">
                  <button className='arrow' onClick={goToBack}><i class="fas fa-arrow-left"></i></button>
                  {/* <button onClick={goToCurrent}>today</button>
                  <button onClick={goToNext}>&#8250;</button> */}
                  <label className="rbc-toolbar-label" >{label()}</label>
                  <button onClick={HandleClick} className="monthvisited">Month</button>
                  <button className="weekbtn">Week</button>
                  <ul className={props.sidebar? style.togglemenu : style.hideNav}>
                      <div className={style.filter}>Calender<img src={cal} alt="cal" className={style.cal}/></div>
                      <li onClick={mytaskarray} className={active === 0 ? style.active: ""}><img src={task} alt="tsk" />My Task <div className="dot">{taskList.length}</div></li>
                      <li onClick={friendtaskarray} className={active === 1 ? style.active: ""}><img src={friend} alt="frn" />Friend Task<div className="dot"> {ftaskList.length}</div></li>
                      <div className={style.filter}>Filter<img src={filter} alt="frn" className={style.logo}/></div>
                      <li onClick={goToBack} className={active === 2 ? style.active: ""}><img src={next} alt="frn"  className={style.prev}/>Previous Month</li>
                      <li onClick={goToNext} className={active === 3 ? style.active: ""}><img src={prev}  alt="frn" className={style.next}/>Next Month</li>
                  </ul> 
                </div>
              </div >
            );
          };
      


          const EventComponent = ({event}) => { 
            return ( 
            <div class="weekevent">
              <div class="wct">
                <img src={event.img} className="profile"/>
                <div className="name">{event.name}</div>
                  {event.title}
                <div className="time">{moment(event.time).format('h:mm A')}</div>
              </div>
            </div>
            
                ) }
    
    
        const CountEvent = (event, date) => { 
                    var count=0;
                    var eventdates= localizer.format(event.start, 'DD mm yyyy');
                    return alert(eventdates);
        } 
                  
    
    
        const customSlotPropGetter = (date) => {
            if(match.find(event =>
              moment(date).isBetween(
                moment(event.start).format('YYYY-MM-DD'),
                moment(event.end).format('YYYY-MM-DD'),
                null,
                "[]"
              )
            ) != undefined)
            
              return {
                className: 'eventDay',
                style: {
                  //border: 'solid 3px ' + (date.getDate() === 6 ? '#faa' : '#afa'),
                  
                 /*  borderRadius: '10px',
                  marginRight: '0',
                  backgroundColor: '#222E3E',
                   */ 
                  
                 
                    
                
                },
              };
            else {}
            };
          


    return (
        <div>

       {/* <Calnav sidebar={props.sidebar} setSidebar={props.setSidebar}/> */}
        <Calendar
        className={props.sidebar? ['rbc-calendar']: ['c1','c2']}
        localizer={localizer}
        events={match}
        startAccessor="start"

        style={{ height: '100vh' }}

        views={{
        month: true,
        week: true,
        
        }}
        defaultView={'week'}
        dayPropGetter={customSlotPropGetter}
        components={{
        event: EventComponent,
        week: {
            header: ({ date, localizer }) => 
            {return <div className="header">
                <div><div className="datelabel">{localizer.format(date, 'DD')} </div>{localizer.format(date, 'ddd')}</div>
            </div>},
           toolbar : CustomToolbar
            
        }
        }
        }
        
        

/>
            
        </div>
    )
}

export default Weekcalender
