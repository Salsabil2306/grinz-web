import {useState, useEffect} from 'react';
import { Calendar, momentLocalizer  } from 'react-big-calendar' 
import moment from 'moment'
import dummyData from '../component/events'
/* import 'react-big-calendar/lib/css/react-big-calendar.css' */
import './Monthcalender.css'
import Toolbar from 'react-big-calendar/lib/Toolbar';
import { Link } from 'react-router-dom';
import Calnav from './Calnav'
import style from './Calnav.module.css'
import archive from '../archived.svg'
import cal from '../calender.svg'
import friend from '../friends.svg'
import task from '../mytask.svg'
import filter from '../Filter.svg'
import next from '../nextMonth.svg'
import prev from '../prevMonth.svg'




export default function Monthcalender(props) {
  
  console.log(props.ftaskList);
  const taskList = props.mytask.map(x => ({ start: moment(x.date), end: moment(x.date), title: x.name }));
  const [eventarr, setEventarr] = useState([...taskList]);
  const [active, setActive] = useState(0);
  
  
 
 
 console.log(props.ftaskList);
  
    
const localizer = momentLocalizer(moment);
  
   

   /*    const events = []
        dummyData.forEach(obj => {
        obj.taskData.forEach(
            item => {
            events.push({
                start: moment(item.date),
                end: moment(item.date),
                title: obj.title
            })
            }
        )
        })    */


       
   
    

    const HandleClick = (event) => {
      event.preventDefault();
      window.location.href='/week';
  
  
    }
    
  
    
    const CustomToolbar = (toolbar, {date}) => {
      const goToBack = () => {
        toolbar.date.setMonth(toolbar.date.getMonth() - 1);
        toolbar.onNavigate('PREV');
        setActive(2);
      };
    
      const goToNext = () => {
        toolbar.date.setMonth(toolbar.date.getMonth() + 1);
        toolbar.onNavigate('NEXT');
        setActive(3);
      };
    
      const goToCurrent = () => {
        const now = new Date();
        toolbar.date.setMonth(now.getMonth());
        toolbar.date.setYear(now.getFullYear());
        toolbar.onNavigate('current');
      };
      
      const mytaskarray = () => {
        console.log(date);
        setEventarr(taskList);
        setActive(0);
      }

      const friendtaskarray = () => {
        setEventarr(props.ftaskList);
        setActive(1);
      }


      const label = () => {
        const date = moment(toolbar.date);
        return (
          <span><b>{date.format('MMMM')}</b><span> {date.format('YYYY')}</span></span>
        );
      };
    
      return (
        <div >
          
    
          <div className="main">
            <button className='arrow' onClick={goToNext}><i class="fas fa-arrow-left"></i></button>
            {/* <button onClick={goToCurrent}>today</button>
            <button onClick={goToNext}>&#8250;</button> */}
            <label className="rbc-toolbar-label" >{label()}</label>
            <button className="mon">Month</button>

            <button onClick={HandleClick} className="visited">Week</button>
            <ul className={props.sidebar? style.togglemenu : style.hideNav}>
                      <div className={style.filter}>Calender<img src={cal} alt="cal" className={style.cal}/></div>
                      <li onClick={mytaskarray} className={active === 0 ? style.active: ""}><img src={task} alt="tsk" />My Task<div className="dot">{taskList.length}</div></li>
                      <li onClick={friendtaskarray} className={active === 1 ? style.active: ""}><img src={friend} alt="frn" />Friend Task<div className="dot">{props.ftaskList.length}</div></li>
                      <div className={style.filter}>Filter<img src={filter} alt="frn" className={style.logo}/></div>
                      <li onClick={goToBack} className={active === 2 ? style.active: ""}><img src={next} alt="frn"  className={style.prev}/>Previous Month</li>
                      <li onClick={goToNext} className={active === 3 ? style.active: ""}><img src={prev}  alt="frn" className={style.next}/>Next Month</li>          
            </ul> 
            
          </div>
        </div >
      );
    };

    const EventComponent = (event) => { 
        return ( 
            <div className="eventList">
              <span className="ct"><i class="far fa-square"></i> {event.title}</span>
            </div>
            
            ) }



    const CountEvent = (event, date) => { 
                document.getElementsByClassName('rbc-event').className = '';
                var count=0;
                var eventdates= localizer.format(event.start, 'DD mm yyyy');
                return alert(eventdates);
    } 
              


    const customDayPropGetter = (date, label) => {
        if(eventarr.find(event =>
          moment(date).isBetween(
            moment(event.start).format('YYYY-MM-DD'),
            moment(event.end).format('YYYY-MM-DD'),
            null,
            "[]"
          )
        ) != undefined)
        
          return {
            className: 'special-day',
            style: {
              //border: 'solid 3px ' + (date.getDate() === 6 ? '#faa' : '#afa'),
              border: 'solid 1px #4E6786',
              borderRadius: '10px',
              backgroundColor:'#222E3E',
              margin: '5px',
            },
          };
        else return {};
        };

    

    return (
        <div>
        
          <Calendar
             popup
             messages={{
               showMore: total => (
                 <div>{`+${total}`}
                 </div>
               ),
             }}
            className={props.sidebar? ['rbc-calendar']: ['c1','c2']}
            popup
            events={eventarr}

            localizer={localizer}
            events={eventarr}
            startAccessor="start"
            style={{ height: '100vh' }}
            onSelectEvent={CountEvent}
            views={{
              month: true,
              week: true,
              
            }}
            dayPropGetter={customDayPropGetter}
            components={{
              month: {
                dateHeader: ({ date, label}) => {
                  if (date.getDay() === 5 || date.getDay() === 6) { return <div className="weekend">{label}</div>; }
                    
                    else if (eventarr.find(event =>
                      moment(date).isBetween(
                        moment(event.start._i).format('YYYY-MM-DD'),
                        moment(event.end._i).format('YYYY-MM-DD'),
                        null,
                        "[]"
                      )
                    ) != undefined) { 
                      var calDate=moment(date).format('YYYY-MM-DD');
                      const result = eventarr.filter(singleTask => moment(singleTask.start._i).format('YYYY-MM-DD')  === calDate);
                      console.log(result.length);

                      return <div className="list">{label}<div className="taskCount">{result.length} tasks</div> </div>; }
                    
                    
                    else { return <div className="label">{label}</div>; }
                },
                
              
          
            
                },  
        

            event: EventComponent,
          
            toolbar : CustomToolbar

          }
          
          }

        />
                  
              </div>
          )
      }
