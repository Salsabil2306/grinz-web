import React, { useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import design from './Topnav.module.css'
import Sidenav from './Sidenav';
import cal from '../calender.svg'
import friend from '../friends.svg'
import task from '../mytask.svg'


function Topnav(props) {
    
     const showSidebar = () => {
        props.setSidebar(!props.sidebar);
     /* console.log(sidebar); */
     
 };
 
     return (
         <div className={design.topnav}>
             <div onClick={showSidebar} className={design.barsbutton}><i class="fas fa-bars"></i></div>

             <Sidenav sidebar={props.sidebar} 
             setSidebar={props.setSidebar}
             user={props.user}
             setUser={props.setUser}
             />
         </div>
     );
 }
export default Topnav
