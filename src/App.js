import './App.css';
import Monthcalender from './component/Monthcalender';
import Weekcalender from './component/Weekcalender';
import {useState, useEffect} from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Sidenav from './component/Sidenav';
import Topnav from './component/Topnav';
import Archived from './component/Archived';
import Mytask from './component/Mytask';
import Friendtask from './component/Friendtask';
import axios from 'axios';
import moment from 'moment';


function App() {
  const [sidebar, setSidebar]= useState(true);
  const [mytask, setMytask]= useState([]);
  const [user, setUser]= useState([]);
  const [contacts, setContact] = useState([]);
  const [friendtask, setFriendtask] = useState([]);
  const id={"userId": "60069bc8b637ec6bc61dbdeb"};
  
  useEffect(()=>{
          axios.post('http://82.165.54.105/api/v1/users/user/id', id, {
            headers: {
              'authentication': `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDA2OWJjOGI2MzdlYzZiYzYxZGJkZWIiLCJwaG9uZSI6Iis4ODAxNjIyMDk3NzE3IiwibmFtZSI6IlNhbHNhYmlsIiwiYXZhdGFyIjoiIiwic3RhdHVzIjoiIiwiaWF0IjoxNjEzNDU3NDQxLCJleHAiOjE2NzY1Mjk0NDF9.iyMOs6SC4pynR0ptYrqGbn7yikHX1-xRtBq0kv_k9AI`
            }
          })
                .then(res => {
                    console.log(res);
                    console.log(res.data.user);
                    setUser(res.data.user);
                    console.log(user);
                    
                  })
                .catch(error => {
                    console.log(error)
                })
        
      }, []
      
      
      
      )

  useEffect(()=>{
        axios.post('http://82.165.54.105/api/v1/users/contacts/all', {}, {
          headers: {
            'authentication': `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDA2OWJjOGI2MzdlYzZiYzYxZGJkZWIiLCJwaG9uZSI6Iis4ODAxNjIyMDk3NzE3IiwibmFtZSI6IlNhbHNhYmlsIiwiYXZhdGFyIjoiIiwic3RhdHVzIjoiIiwiaWF0IjoxNjEzNDU3NDQxLCJleHAiOjE2NzY1Mjk0NDF9.iyMOs6SC4pynR0ptYrqGbn7yikHX1-xRtBq0kv_k9AI`
          }
        })
              .then(res => {
                  console.log(res.data.data);
                  setContact(res.data.data.user);
                })
              .catch(error => {
                  console.log(error)
              })
      
    }, []
    
    
    
    )
      
        
  
 
  
  useEffect(()=>{
    axios.get('http://82.165.54.105/api/v1/task/all/my', {
      headers: {
        'authentication': `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDA2OWJjOGI2MzdlYzZiYzYxZGJkZWIiLCJwaG9uZSI6Iis4ODAxNjIyMDk3NzE3IiwibmFtZSI6IlNhbHNhYmlsIiwiYXZhdGFyIjoiIiwic3RhdHVzIjoiIiwiaWF0IjoxNjEzNDU3NDQxLCJleHAiOjE2NzY1Mjk0NDF9.iyMOs6SC4pynR0ptYrqGbn7yikHX1-xRtBq0kv_k9AI`
      }
    })
    
    .then((response)=>{
        console.log(response.data);
        setMytask(response.data.data.taskList);
        
        
        
        
        
    })
}, []



)


useEffect(()=>{
  axios.get('http://82.165.54.105/api/v1/task/all/friend', {
    headers: {
      'authentication': `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDA2OWJjOGI2MzdlYzZiYzYxZGJkZWIiLCJwaG9uZSI6Iis4ODAxNjIyMDk3NzE3IiwibmFtZSI6IlNhbHNhYmlsIiwiYXZhdGFyIjoiIiwic3RhdHVzIjoiIiwiaWF0IjoxNjEzNDU3NDQxLCJleHAiOjE2NzY1Mjk0NDF9.iyMOs6SC4pynR0ptYrqGbn7yikHX1-xRtBq0kv_k9AI`
    }
  })
  
  .then((response)=>{
      console.log(response.data.data.taskList);
      setFriendtask(response.data.data.taskList);
      
  })
}, []



)



console.log(user);
/* const userName=user.map(x => ({ name: x.name, avatar: x.avatar, id:x._id})); */
const ftaskList = friendtask.map(x => ({ start: moment(x.date), end: moment(x.date), title: x.name }));
const contactList=contacts.map(y => ({ name: y.name, avatar: y.avatar, id:y._id}));
     return (
       <Router>
       <div className="App">
             <Topnav sidebar={sidebar} setSidebar={setSidebar} user={user} setUser={setUser}/>
              <Route exact path="/friendtask" component={Friendtask}/>
              <Route exact path="/archive" component={Archived}/>
              <Route exact path="/mytask" render={(props) => (<Mytask sidebar={sidebar} setSidebar={setSidebar} mytask={mytask} setMytask={setMytask} user={user} setUser={setUser} contactList={contactList}/>)}/>
              <Route exact path="/month" render={(props) => (<Monthcalender sidebar={sidebar} setSidebar={setSidebar} mytask={mytask} setMytask={setMytask} ftaskList={ftaskList} />)}/>
              <Route exact path="/week" render={(props) => (<Weekcalender sidebar={sidebar} setSidebar={setSidebar} mytask={mytask} setMytask={setMytask} user={user} setUser={setUser} contactList={contactList} friendtask={friendtask} setFriendtask={setFriendtask}/>)}/>
             
       </div>
 
       </Router>
      
     );
   }
 

export default App;
